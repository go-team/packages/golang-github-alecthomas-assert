Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/alecthomas/assert
Upstream-Name: assert
Upstream-Contact: https://github.com/alecthomas/assert/issues/new

Files: *
Copyright: 2021 Alec Thomas
License: Expat
Comment:
 github.com/alecthomas/assert v1.0.0 and older was forked by Alec Thomas
 in 2015 from github.com/stretchr/testify/assert which was
 Copyright 2012–2013 by Mat Ryer and Tyler Bunnell.  assert/v2, on the
 other hand, is a new clean-room implementation using Go 1.18 generics.

Files: debian/*
Copyright: 2017 Anthony Fok <foka@debian.org>
License: Expat
Comment: Debian packaging is licensed under the same terms as upstream

License: Expat
 Please consider promoting this project if you find it useful.
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
